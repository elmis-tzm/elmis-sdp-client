package org.elmis.util;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static Date fromLocalDate(LocalDate localDate) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, localDate.getDayOfMonth());
		// Localdate.Month.getValue() returns +1,
		// therefore, we have to subtract 1 to get the correct value
		int month = localDate.getMonth().getValue() - 1;
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, localDate.getYear());
		return cal.getTime();
	}
}
