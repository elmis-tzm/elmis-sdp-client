package org.elmis.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.elmis.model.Adjustment;
import org.elmis.model.Elmis_Stock_Control_Card;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class AdjustmentUtil {

	private static final Logger log = Logger.getLogger(AdjustmentUtil.class);

	private static boolean isValid = false;
	private List<Elmis_Stock_Control_Card> stcList;
	private Elmis_Stock_Control_Card stc;

	private Double stockQty = 0.0;
	private Double additions = 0.0;
	private Double deductions = 0.0;
	private Double netBalance = 0.0;

	private static AdjustmentUtil instance = new AdjustmentUtil();

	private AdjustmentUtil() {

	}

	/**
	 * 
	 * @return
	 */
	public static AdjustmentUtil getInstance() {
		return instance;
	}

	/**
	 * 
	 * @param adjustments
	 * @param selectedProduct
	 * @param transferedFromFacility
	 * @param transferedToFacility
	 * @return
	 */
	public boolean isAdjustmentValid(List<Adjustment> adjustments,
			FacilityApprovedProductResult selectedProduct,
			String transferedFromFacility, String transferedToFacility,
			Date adjustmentDate, Double adjustmentFromPhyCount) {
		stcList = new ArrayList<>();
		stockQty = selectedProduct.getQuantity();
		//reset these values before calculating the net balance
		additions = 0.0;
		deductions = 0.0;

		for (Adjustment adj : adjustments) {
			stc = new Elmis_Stock_Control_Card();
			if (adj.getAdjustmentQty() != 0.0) {
				// HARD CODED SHOULD BE REMOVED
				stc.setDpid(87);
				stc.setProductcode(selectedProduct.getProductCode());
				stc.setProductid(selectedProduct.getProductId());
				stc.setId(UUID.randomUUID().toString());
				stc.setRemark(adj.getAdjustmentRemark());
				stc.setStoreroomadjustment(true);
				stc.setProgram_area(selectedProduct.getProgramCode());
				stc.setAdjustmenttype(adj.getAdjustmentType());
				// HARD CODED SHOULD BE REMOVED
				stc.setCreatedby("Mekbib Awoke");
				stc.setCreateddate(new Timestamp(adjustmentDate.getTime()));
				if (adj.getAdditive()) {
					additions += adj.getAdjustmentQty();
					stc.setBalance(stockQty + additions);
					stc.setAdjustments(adj.getAdjustmentQty());
				} else {
					deductions -= adj.getAdjustmentQty();
					stc.setBalance(stockQty + deductions);
					stc.setAdjustments(-adj.getAdjustmentQty());
				}

				if (transferedFromFacility != null) {
					stc.setIssueto_receivedfrom(transferedFromFacility);
				}else{
					stc.setIssueto_receivedfrom(adj.getAdjustmentType());
				}

				if (transferedToFacility != null) {
					stc.setIssueto_receivedfrom(transferedToFacility);
				}else{
					stc.setIssueto_receivedfrom(adj.getAdjustmentType());
				}
				
				if (adjustmentFromPhyCount != 0) {
					stc.setIssueto_receivedfrom(adj.getAdjustmentType());
				}

				// add the stc object to stcList
				stcList.add(stc);
			}
		}
		
		
		netBalance = (stockQty + additions + deductions);
		
		//accommodates for adjustments from physical count
		if(adjustmentFromPhyCount != 0){
			if(netBalance == (stockQty + adjustmentFromPhyCount) ){
				return true;
			}
			return false;			
		}
		
		
		if (netBalance >= 0.0) {
			// commence the transaction
			isValid = true;
			log.debug("adjustment is valid COMMENCE!");
		}else{
			isValid = false;
			log.debug("adjustment is NOT valid!");
		}
		
		log.debug(" Stock = " + stockQty + " Additions = " + additions
				+ " deductions = " + deductions + " Netbalance = " + netBalance);

		return isValid;
	}

	/**
	 * 
	 * @return
	 */
	public List<Elmis_Stock_Control_Card> getStockControlCardList() {
		return stcList;
	}

	/**
	 * 
	 * @return
	 */
	public Double getNetBalance() {
		return netBalance;
	}
}
