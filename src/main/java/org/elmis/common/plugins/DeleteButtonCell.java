package org.elmis.common.plugins;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

import org.apache.log4j.Logger;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;

public class DeleteButtonCell extends TableCell<Object, Boolean> {
	
	private static final Logger log = Logger.getLogger(DeleteButtonCell.class);

    final Button cellButton = new Button("Delete");

    public DeleteButtonCell() {

        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                TableRow theRow = (TableRow) getParent();

                TableView<Object> theTable = theRow.getTableView();
                Action response = Dialogs
    					.create()
    					.owner(ELMISFacility.mainStage)
    					.title("Warning")
    					.masthead("Confirm Dialog")
    					.message(
    							"Are you sure, you want to delete this record?")
    					.showConfirm();
    			if (response == Dialog.Actions.YES) {
    				theTable.getItems().remove(getIndex());
    			}
            }

        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean item, boolean empty) {
    	super.updateItem(item, empty);
    	
    	if(empty){
    		setText(null);
    		setGraphic(null);
    	}else{
    		setGraphic(cellButton);
    		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    	}
    	
    }
}
