/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.common.plugins;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 *
 * @author Mekbib
 */
public class TextFieldLimitCharsInputListener implements ChangeListener<String> {

    public static String NUMBERS_15_DIGITS = "[0-9]{15}";

    //number of characters to limit input to
    private int maxLength = -1;

    //regular expression to restrict input string
    private String inputType;

    //TextField to attach event to
    TextField eventOwner;

    public TextFieldLimitCharsInputListener(TextField eventOwner) {
        this.eventOwner = eventOwner;
        this.eventOwner.textProperty().addListener(this);
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

        if (newValue == null) {
            return;
        }

        if (maxLength > -1 && newValue.length() > maxLength) {
            eventOwner.setText(newValue.substring(0, maxLength));
        }

        if (inputType != null) {
            if (!inputType.equals("") && !newValue.matches(inputType)) {
                eventOwner.setText(oldValue);
            }
        }
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

}
