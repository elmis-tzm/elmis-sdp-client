package org.elmis.common.plugins;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import org.apache.log4j.Logger;

/**
 *
 * @author Graham Smith
 */
public abstract class DateEditingCell extends TableCell<Object, Date> {

	private static final Logger log = Logger.getLogger(DateEditingCell.class);

	private final SimpleDateFormat formatter;
	protected DatePicker datePicker;

	public DateEditingCell() {

		formatter = new SimpleDateFormat("MM/dd/yyyy");
		datePicker = new DatePicker();
		//bind change event to set bean property 
		//which will be implemented in the subclass
		createListenerForDatePicker();
		
		// Commit edit on Enter and cancel on Escape.
		// Note that the default behavior consumes key events, so we must
		// register this as an event filter to capture it.
		// Consequently, with Enter, the datePicker's value won't yet have been
		// updated,
		// so commit will sent the wrong value. So we must update it ourselves
		// from the
		// editor's text value.

		datePicker.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
			if (event.getCode() == KeyCode.ENTER
					|| event.getCode() == KeyCode.TAB) {
				datePicker.setValue(datePicker.getConverter().fromString(
						datePicker.getEditor().getText()));

				commitEdit(getDateFromLocalDate(datePicker.getValue()));
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				cancelEdit();
			}
		});

		// Modify default mouse behavior on date picker:
		// Don't hide popup on single click, just set date
		// On double-click, hide popup and commit edit for editor
		// Must consume event to prevent default hiding behavior, so
		// must update date picker value ourselves.

		// Modify key behavior so that enter on a selected cell commits the edit
		// on that cell's date.

		datePicker.setDayCellFactory(picker -> {
			DateCell cell = new DateCell();
			cell.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
				datePicker.setValue(cell.getItem());
				if (event.getClickCount() == 1) {
					datePicker.hide();
					commitEdit(getDateFromLocalDate(cell.getItem()));
				}
				event.consume();
			});
			cell.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				if (event.getCode() == KeyCode.ENTER) {
					commitEdit(getDateFromLocalDate(datePicker.getValue()));
				}
			});
			return cell;
		});

		contentDisplayProperty().bind(
				Bindings.when(editingProperty())
						.then(ContentDisplay.GRAPHIC_ONLY)
						.otherwise(ContentDisplay.TEXT_ONLY));
	}

	@Override
	public void updateItem(Date expireDate, boolean empty) {
		super.updateItem(expireDate, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (null == expireDate) {
				setText("");
			} else {
				setText(formatter.format(expireDate));
			}
			setGraphic(datePicker);
		}
	}

	@Override
	public void startEdit() {
		super.startEdit();
		if (!isEmpty()) {
			datePicker.setValue(LocalDate.now());
		}
	}

	protected Date getDateFromLocalDate(LocalDate localDate) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, localDate.getDayOfMonth());
		//Localdate.Month.getValue() returns +1,
		//therefore, we have to subtract 1 to get the correct value
		int month = localDate.getMonth().getValue() - 1;
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, localDate.getYear());
		return cal.getTime();
	}

	
	protected abstract void createListenerForDatePicker();
	public abstract Callback getCellFactory();
}