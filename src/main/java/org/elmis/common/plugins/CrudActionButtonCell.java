package org.elmis.common.plugins;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;


public class CrudActionButtonCell extends TableCell<Object, Boolean> {
	
	private Stage parentStage;

	private static final Logger log = Logger
			.getLogger(CrudActionButtonCell.class);

	final MenuButton actionButton = new MenuButton();
	final MenuItem viewButton = new MenuItem("View");
	final MenuItem editButton = new MenuItem("Edit");
	final MenuItem deleteButton = new MenuItem("Delete");



	public CrudActionButtonCell(Stage parentStage) {
		//set the parent stage 
		this.parentStage = parentStage;
		actionButton.getItems().addAll(viewButton, editButton, deleteButton);

		viewButton.setOnAction(new CrudActionHandler("view"));
		editButton.setOnAction(new CrudActionHandler("edit"));
		deleteButton.setOnAction(new CrudActionHandler("delete"));

	}

	// Display button if the row is not empty
	@Override
	protected void updateItem(Boolean item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(actionButton);
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		}

	}

	class CrudActionHandler implements EventHandler<ActionEvent> {
		private String opType;

		@SuppressWarnings("rawtypes")
		@Override
		public void handle(ActionEvent event) {
			TableRow theRow = (TableRow) getParent();
			
			// do the conditional checks here
			if (opType.toLowerCase().equals("view")) {
				@SuppressWarnings("unchecked")
				TableView<Object> theTable = theRow.getTableView();
				DialogWindow.getInstance().openDialog(
						ELMISFacility.class
								.getResource("views/admin/productsource/AddProductSourceController.fxml"),
						"Add Product Source", parentStage);
				
			}
			
			if (opType.toLowerCase().equals("delete")) {
				@SuppressWarnings("unchecked")
				TableView<Object> theTable = theRow.getTableView();
				Action response = Dialogs
						.create()
						.owner(ELMISFacility.mainStage)
						.title("Warning")
						.masthead("Confirm Dialog")
						.message(
								"Are you sure, you want to delete this record?")
						.showConfirm();
				if (response == Dialog.Actions.YES) {
					theTable.getItems().remove(getIndex());
				}
			}
			
			
			if (opType.toLowerCase().equals("edit")) {
				@SuppressWarnings("unchecked")
				TableView<Object> theTable = theRow.getTableView();
				DialogWindow.getInstance().openDialog(
							ELMISFacility.class
									.getResource("views/admin/productsource/AddProductSourceController.fxml"),
							"Add Product Source", parentStage);
				
			}

		}
		
		

		protected CrudActionHandler(String operation) {
			opType = operation;
		}

	}
}
