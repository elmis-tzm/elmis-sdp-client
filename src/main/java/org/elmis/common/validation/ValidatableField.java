package org.elmis.common.validation;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.elmis.common.validation.ValidationUtil.Type;

/**
 * 
 * @author mawoke
 *
 */
public class ValidatableField {
	private Control field;
	private Type type;
	private String fieldName;

	public ValidatableField(Control field, Type type, String fieldName) {
		super();
		this.field = field;
		this.type = type;
		this.fieldName = fieldName;
	}

	public Control getField() {
		return field;
	}

	public void setField(Control field) {
		this.field = field;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		String tempValue = null;

		if (field instanceof TextField) {
			tempValue = ((TextField) field).getText() == null ? null
					: ((TextField) field).getText();
		}

		if (field instanceof TextArea) {
			tempValue = ((TextArea) field).getText() == null ? null
					: ((TextArea) field).getText();
		}

		if (field instanceof PasswordField) {
			tempValue = ((PasswordField) field).getText() == null ? null
					: ((PasswordField) field).getText();
		}

		if (field instanceof DatePicker) {
			tempValue = ((DatePicker) field).getValue() == null ? null
					: ((DatePicker) field).getValue().toString();
		}

		if (field instanceof ComboBox) {
			tempValue = ((ComboBox) field).getValue() == null ? null
					: ((ComboBox) field).getValue().toString();
		}
		return tempValue;
	}

}
