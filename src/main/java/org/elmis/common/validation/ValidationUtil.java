package org.elmis.common.validation;

import java.util.ArrayList;

import javafx.scene.control.Control;

/**
 * 
 * @author mawoke
 *
 */
public class ValidationUtil {

	public static enum Type {
		REQUIRED, EMAIL, NUMBER, PHONE
	};

	private static ArrayList<ValidatableField> fieldRegister = new ArrayList<ValidatableField>();
	private static StringBuilder validationMessage = null;

	/**
	 * taken from http://regexlib.com/
	 */
	private static String EMAIL_PATTERN = "^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$";
	private static String NUMBER_PATTERN = "^\\d+$";
	private static String PHONE_PATTERN = "^(\\+[1-9][0-9]*(\\([0-9]*\\)|-[0-9]*-))?[0]?[1-9][0-9\\- ]*$";

	/**
	 * 
	 * @param field
	 *            , which is going to be validated
	 * @param type
	 *            , the type of validation to be applied
	 * @param fieldName
	 *            , the name of the field
	 */
	public static void register(Control field, Type type, String fieldName) {
		fieldRegister.add(new ValidatableField(field, type, fieldName));
	}

	/**
	 * validates the fields in {@link #fieldRegister}
	 * 
	 * @return boolean, <b>true</b> if valid, <b>false</b> otherwise
	 */
	public static boolean isValid() {
		boolean valid = true;
		validationMessage = new StringBuilder("");
		for (ValidatableField field : fieldRegister) {

			if (field.getFieldValue() == null
					|| field.getFieldValue().equals("")) {
				valid = false;
				validationMessage.append(field.getFieldName() + " is required")
						.append("\n");
			} else {
				if (field.getType() == Type.EMAIL) {

					// do the validation
					if (!field.getFieldValue().matches(EMAIL_PATTERN)) {
						valid = false;
						validationMessage.append(
								field.getFieldName() + " is invalid email")
								.append("\n");
					} else {
						valid = true;
					}
				}

				if (field.getType() == Type.NUMBER) {

					// do the validation
					if (!field.getFieldValue().matches(NUMBER_PATTERN)) {
						valid = false;
						validationMessage.append(
								field.getFieldName() + " is invalid number")
								.append("\n");
					} else {
						valid = true;
					}

				}
			}

		}

		return valid;
	}

	/**
	 * return the validation message
	 * 
	 * @return
	 */
	public static String message() {
		return validationMessage.toString();
	}

	/**
	 * 
	 */
	public static void resetValidtor() {
		// reset to avoid duplicate validation
		fieldRegister.clear();
	}

}
