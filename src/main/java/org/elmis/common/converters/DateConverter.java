/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.elmis.common.converters;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Mekbib
 */
public class DateConverter {  
    
    
    public static Timestamp fromDateToTimestamp(Date date){
        return new Timestamp( date.getTime()) ;
    }
    
    
}
