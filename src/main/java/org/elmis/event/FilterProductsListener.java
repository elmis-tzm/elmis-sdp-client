package org.elmis.event;

import java.util.function.Predicate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;

import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class FilterProductsListener implements ChangeListener<String> {

	FilteredList<FacilityApprovedProductResult> filteredProductList;

	/**
	 * set {@link #filteredProductList } to be filtered
	 * 
	 * @param filteredProductList
	 */
	public FilterProductsListener(
			FilteredList<FacilityApprovedProductResult> filteredProductList) {
		this.filteredProductList = filteredProductList;
	}

	@Override
	public void changed(ObservableValue<? extends String> paramObservableValue,
			String oldSearchText, String newSearchText) {
		filteredProductList
				.setPredicate(new Predicate<FacilityApprovedProductResult>() {

					@Override
					public boolean test(FacilityApprovedProductResult product) {

						// If filter text is empty, display all
						// products.
						if (newSearchText == null || newSearchText.isEmpty()) {
							return true;
						}

						// Compare product name and last name of
						// every
						// product with filter text.
						String lowerCaseFilter = newSearchText.toLowerCase();

						if (product.getProductName().toLowerCase()
								.indexOf(lowerCaseFilter) != -1) {
							return true; // Filter matches
											// product name.
						} else if (product.getProductCode().toLowerCase()
								.indexOf(lowerCaseFilter) != -1) {
							return true; // Filter matches
											// product code.
						}
						return false; // Does not match.
					}
				});

	}

}
