package org.elmis.service.interfaces;

import java.util.ArrayList;

import org.elmis.model.NodeProduct;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public interface IProduct {
	public abstract ArrayList<FacilityApprovedProductResult> getFacilityApprovedProducts();
	public abstract ArrayList<NodeProduct> getFacilityApprovedNodeProducts(String programCode);
}
