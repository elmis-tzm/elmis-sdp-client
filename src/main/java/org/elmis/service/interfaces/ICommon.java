package org.elmis.service.interfaces;

import java.util.ArrayList;

import org.elmis.model.Program;

public interface ICommon {
	public abstract ArrayList<Program> getPrograms();
}
