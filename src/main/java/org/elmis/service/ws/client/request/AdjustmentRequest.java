package org.elmis.service.ws.client.request;

import java.util.Date;

import org.elmis.model.LossAdjustmentType;
import org.elmis.model.Node;

public class AdjustmentRequest {
	
	Node node;
	String productCode;
	LossAdjustmentType laType;
	Date date;
	Double quantity;
	Integer userId;
	public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public LossAdjustmentType getLaType() {
		return laType;
	}
	public void setLaType(LossAdjustmentType laType) {
		this.laType = laType;
	}
	
}
