package org.elmis.service.ws.client.request;

import java.util.Date;
import java.util.List;

import org.elmis.model.Node;
import org.elmis.model.TransactionProduct;

public class IssueProductsRequest {
	
	private Node store;
	private Node dispensingPoint;
	private List<TransactionProduct> productsIssued;
	private Long txnDate;
	private Integer userId;
	public Node getStore() {
		return store;
	}
	public void setStore(Node store) {
		this.store = store;
	}
	public Node getDispensingPoint() {
		return dispensingPoint;
	}
	public void setDispensingPoint(Node dispensingPoint) {
		this.dispensingPoint = dispensingPoint;
	}
	public Long getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Long txnDate) {
		this.txnDate = txnDate;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<TransactionProduct> getProductsIssued() {
		return productsIssued;
	}
	public void setProductsIssued(List<TransactionProduct> productsIssued) {
		this.productsIssued = productsIssued;
	}
}
