package org.elmis.service.ws.client.request;

import org.elmis.model.Node;

public class StockControlCardRequest {

	Node store;
	Long from;
	Long to;
	String productCode;
	public Node getStore() {
		return store;
	}
	public void setStore(Node store) {
		this.store = store;
	}
	public Long getFrom() {
		return from;
	}
	public void setFrom(Long from) {
		this.from = from;
	}
	public Long getTo() {
		return to;
	}
	public void setTo(Long to) {
		this.to = to;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
}
