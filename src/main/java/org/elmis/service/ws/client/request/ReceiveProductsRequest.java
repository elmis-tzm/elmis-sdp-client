package org.elmis.service.ws.client.request;

import java.util.List;

import org.elmis.model.Node;
import org.elmis.model.TransactionProduct;

public class ReceiveProductsRequest {
	
	private Node store;
	private List<TransactionProduct> productsReceived;
	private Long txnDate;
	private Integer userId;
	public Node getStore() {
		return store;
	}
	public void setStore(Node store) {
		this.store = store;
	}
	public List<TransactionProduct> getProductsReceived() {
		return productsReceived;
	}
	public void setProductsReceived(List<TransactionProduct> productsReceived) {
		this.productsReceived = productsReceived;
	}
	public Long getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(Long txnDate) {
		this.txnDate = txnDate;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
