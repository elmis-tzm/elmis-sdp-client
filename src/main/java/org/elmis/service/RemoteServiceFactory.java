package org.elmis.service;

import org.elmis.service.interfaces.ICommon;
import org.elmis.service.interfaces.INode;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.interfaces.IUser;
import org.elmis.service.remote.CommonService;
import org.elmis.service.remote.NodeService;
import org.elmis.service.remote.ProductService;
import org.elmis.service.remote.StoreService;

/**
 * 
 * @author mekbib
 *
 */
public class RemoteServiceFactory implements ServiceFactory{

	@Override
	public IStore getStoreService() {
		// TODO Auto-generated method stub
		return new StoreService();
	}

	@Override
	public IUser getUserService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProduct getProductService() {
		
		return new ProductService();
	}

	@Override
	public INode getNodeService() {
		
		return new NodeService();
	}

	@Override
	public ICommon getCommonService() {
		// TODO Auto-generated method stub
		return new CommonService();
	}

	

}
