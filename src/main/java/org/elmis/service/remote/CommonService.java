package org.elmis.service.remote;

import java.util.ArrayList;

import org.elmis.model.Program;
import org.elmis.service.interfaces.ICommon;
import org.elmis.service.ws.client.WSClientCommon;

public class CommonService implements ICommon{

	@Override
	public ArrayList<Program> getPrograms() {
		return WSClientCommon.getInstance().getPrograms();
	}

}
