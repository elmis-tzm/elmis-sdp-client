package org.elmis.service.remote;

import java.util.ArrayList;

import org.elmis.model.NodeProduct;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.ws.client.WSClientProduct;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class ProductService implements IProduct {

	@Override
	public ArrayList<FacilityApprovedProductResult> getFacilityApprovedProducts() {
		return WSClientProduct.getInstance().getFacilityApprovedProducts();
	}
	
	@Override
	public ArrayList<NodeProduct> getFacilityApprovedNodeProducts(
			String programCode) {
		return WSClientProduct.getInstance().getFacilityApprovedNodeProducts(
				programCode);
	}

	public static void main(String[] args) {
		System.out.println("No of facility approved products returned = "
				+ new ProductService().getFacilityApprovedNodeProducts("ARV").size());
	}
}
