package org.elmis.views.productsource;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.common.plugins.CrudActionButtonCell;
import org.elmis.facility.domain.model.ProductSource;
import org.elmis.resolver.ServiceResolver;

public class ProductSourceController extends DialogWindow implements
		Initializable {

	@FXML
	public Button btnAdd;

	@FXML
	public Button btnList;

	@FXML
	public TableView<ProductSource> tblProductSources;

	@FXML
	public TableColumn<ProductSource, String> colCreatedBy;

	@FXML
	public TableColumn<ProductSource, String> colSourceName;

	@FXML
	public TableColumn<ProductSource, String> colCreatedDate;

	@FXML
	public TableColumn<ProductSource, String> colAction;

	private ObservableList<ProductSource> productSourceList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// initialize product source table
		initProductSourceTable();
	}

	/**
	 * 
	 */

	private void initProductSourceTable() {

		colCreatedBy
				.setCellValueFactory(new PropertyValueFactory<>("createdby"));
		colSourceName.setCellValueFactory(new PropertyValueFactory<>(
				"sourcename"));
		// colCreatedDate.setCellValueFactory(new
		// PropertyValueFactory<>("createddate"));

		colCreatedDate
				.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ProductSource, String>, ObservableValue<String>>() {
					@Override
					public ObservableStringValue call(
							CellDataFeatures<ProductSource, String> features) {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"dd-MM-YYYY");
						String str = sdf.format(features.getValue()
								.getCreateddate());
						return new SimpleStringProperty(str);
					}
				});

		colAction
				.setCellFactory(new Callback<TableColumn<ProductSource, String>, TableCell<ProductSource, String>>() {


			@SuppressWarnings("rawtypes")
			@Override
			public TableCell call(
					TableColumn<ProductSource, String> p) {
				return new CrudActionButtonCell(dialogStage);
			}


				});

		productSourceList = FXCollections.observableList(ServiceResolver
				.getServiceFactory().getProductsource().getAllProductSource());

		// adding a single object to the table internal list
		tblProductSources.setItems(productSourceList);

	}

	public void addviewproductSourceBtnActionHandler(ActionEvent e){
		
	}
	
	public void addproductSourceBtnActionHandler(ActionEvent e) {

		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/admin/productsource/AddProductSource.fxml"),
				"Add Product Source", dialogStage);

	}

}
