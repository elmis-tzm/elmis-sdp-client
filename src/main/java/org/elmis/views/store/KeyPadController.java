package org.elmis.views.store;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import org.controlsfx.dialog.Dialogs;
import org.elmis.common.DialogWindow;
import org.elmis.common.IntegerTextField;

public class KeyPadController extends DialogWindow implements Initializable {

	public static Double quantityInStock = 0.0;
	private static Double tempQuantityEntered = 0.0;
	private static Double quantityEntered = 0.0;
	public static String programCode;
	public static String productCode;

	private IntegerTextField txtQuantity;

	@FXML
	private VBox vboxFormContainer;

	@FXML
	private Label lblStockInfo;

	@Override
	public void initialize(URL paramURL, ResourceBundle paramResourceBundle) {

		lblStockInfo.setText("Items in Stock : " + quantityInStock);

		txtQuantity = new IntegerTextField();
		txtQuantity.setAlignment(Pos.CENTER);
		txtQuantity.setStyle("-fx-font-size : 30px");
		txtQuantity.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(
					ObservableValue<? extends String> paramObservableValue,
					String oldValue, String newValue) {

				try {
					tempQuantityEntered = Double.valueOf(newValue);

					if (tempQuantityEntered > quantityInStock) {

						Dialogs myDialog = Dialogs.create().owner(dialogStage)
								.title("Issuing Products")
								.masthead("Information Dialog")
								.message("You are trying to issue over limit!");
						myDialog.showError();
						// reset the value
						txtQuantity.textProperty().setValue(oldValue);
						tempQuantityEntered = Double.valueOf(oldValue);

					}
				} catch (Exception ex) {
					// consume the exception
				}

			}
		});

		txtQuantity.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent paramT) {
				closeKeyPad();
			}
		});

		vboxFormContainer.getChildren().add(0, txtQuantity);
	}

	/**
	 * 
	 * @param contineEvent
	 */
	@FXML
	public void handleContinue(ActionEvent contineEvent) {
		closeKeyPad();
	}

	/**
	 * returns what has been entered on the keypad
	 * 
	 * @return
	 */
	public static Double getEnteredQuantity() {
		return quantityEntered;
	}

	/**
	 * clears the key pad values
	 * 
	 * @return
	 */
	public static void clear() {
		quantityEntered = tempQuantityEntered = 0.0;
	}

	private void closeKeyPad() {
		quantityEntered = tempQuantityEntered;
		this.dialogStage.close();
	}

}
