/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.elmis.views.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.log4j.Logger;
import org.controlsfx.dialog.Dialogs;
import org.elmis.common.DialogWindow;
import org.elmis.common.IntegerTextField;
import org.elmis.common.plugins.NumberEditingCell;
import org.elmis.common.plugins.StringEditingCell;
import org.elmis.facade.ProductFacade;
import org.elmis.model.Adjustment;
import org.elmis.model.Elmis_Stock_Control_Card;
import org.elmis.model.LossAdjustmentType;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.ICommon;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;
import org.elmis.util.AdjustmentUtil;

/**
 * FXML Controller class
 *
 * @author mekbib
 */
public class RecordAdjustmentController extends DialogWindow {

	private static final Logger log = Logger.getLogger(RecordAdjustmentController.class);
	ICommon commonService = ServiceResolver.getInstance().getServiceFactory()
			.getCommonService();
	IProduct productService = ServiceResolver.getInstance().getServiceFactory()
			.getProductService();
	IStore storeService = ServiceResolver.getInstance().getServiceFactory()
			.getStoreService();
	
	ProductFacade productFacade = ProductFacade.getInstance();
	
	// product for which adjustment will be made
	public static FacilityApprovedProductResult selectedProduct;
	public static Double adjustmentFromPhyCount = 0.0;
	// defaults to today
	public static Date adjustmentDate = new Date();
	
	private static boolean validForm = true;
	private static boolean adjustmentSuccessful = false;

	@FXML
	public Label lblProductName;

	@FXML
	public Label lblStockQuantity;

	@FXML
	public TableView<LossAdjustmentType> tblAdjustments;

	@FXML
	public TableColumn<LossAdjustmentType, String> colAdjustmentType;

	@FXML
	public TableColumn<LossAdjustmentType, Double> colQuantity;

	@FXML
	public TableColumn<LossAdjustmentType, String> colRemark;

	@FXML
	public CheckBox chkTransferIn;

	@FXML
	public CheckBox chkTransferOut;

	@FXML
	public ComboBox<Node> cmbTransferIn;

	@FXML
	public ComboBox<Node> cmbTransferOut;

	private List<LossAdjustmentType> adjustmentTypeList;

	private List<LossAdjustmentType> adjustmentList;

	private ObservableList<LossAdjustmentType> adjustmentObservableList;

	private List<Node> faciltiyNameList;

	private String fromFacility = null;
	private String toFacility = null;

	private boolean transferInOrOutCellEdited = false;

	/**
	 * Initializes the controller class.
	 */
	public void initAll() {
		lblProductName.setText("Adjustment for : "
				+ selectedProduct.getProductName());

		lblStockQuantity.setText("Stock Quantity : "
				+ selectedProduct.getQuantity());

		// initialize the adjustments table
		initAdjustmentsTable();
		// initialize the transfer options
		intiTransferOptions();
	}

	/**
     * 
     */
	public void initAdjustmentsTable() {

		colAdjustmentType.setCellValueFactory(new PropertyValueFactory<>(
				"adjustmentType"));

		colAdjustmentType
				.setCellValueFactory(new Callback<CellDataFeatures<Adjustment, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(
							CellDataFeatures<Adjustment, String> data) {
						// adjustment type to be descriptive
						StringProperty adjustmentType;

						if (data.getValue().getAdditive()) {
							adjustmentType = new SimpleStringProperty(data
									.getValue().getAdjustmentType() + " ( + )");
						} else {
							adjustmentType = new SimpleStringProperty(data
									.getValue().getAdjustmentType() + " ( - )");
						}

						return adjustmentType;
					}
				});
		colQuantity.setCellValueFactory(new PropertyValueFactory<>(
				"adjustmentQty"));

		colQuantity.setCellFactory(new AdjustmentQuantityCell()
				.getCellFactory());

		colRemark.setCellValueFactory(new PropertyValueFactory<>(
				"adjustmentRemark"));

		colRemark.setCellFactory(new AdjustmentRemarkCell().getCellFactory());

		// read adjustment types from the database
		adjustmentTypeList = ServiceResolver.getServiceFactory()
				.getFacilityService().fetchAdjustments();

		adjustmentList = new ArrayList<>();
		Adjustment adjRecord = null;

		for (Losses_Adjustments_Types adjType : adjustmentTypeList) {

			adjRecord = new Adjustment();

			adjRecord.setAdjustmentType(adjType.getName());
			adjRecord.setProductCode(selectedProduct.getProductCode());
			adjRecord.setAdditive(adjType.getAdditive());

			adjustmentList.add(adjRecord);

		}

		adjustmentObservableList = FXCollections.observableList(adjustmentList);

		tblAdjustments.setItems(adjustmentObservableList);
	}

	/**
	 * 
	 */
	public void intiTransferOptions() {

		faciltiyNameList = ServiceResolver.getServiceFactory()
				.getFacilityService().getfacilityNames();

		cmbTransferIn.setItems(FXCollections.observableList(faciltiyNameList));
		initPropertyCellFactory(cmbTransferIn);
		cmbTransferIn.setDisable(true);

		cmbTransferOut.setItems(FXCollections.observableList(faciltiyNameList));
		initPropertyCellFactory(cmbTransferOut);
		cmbTransferOut.setDisable(true);

		chkTransferIn.selectedProperty().addListener(
				new ChangeListener<Boolean>() {

					@Override
					public void changed(
							ObservableValue<? extends Boolean> paramObservableValue,
							Boolean isSelectedOld, Boolean isSelectedNew) {
						if (isSelectedNew) {
							cmbTransferIn.setDisable(false);
						} else {
							cmbTransferIn.setDisable(true);
						}

					}
				});

		chkTransferOut.selectedProperty().addListener(
				new ChangeListener<Boolean>() {

					@Override
					public void changed(
							ObservableValue<? extends Boolean> paramObservableValue,
							Boolean isSelectedOld, Boolean isSelectedNew) {
						if (isSelectedNew) {
							cmbTransferOut.setDisable(false);
						} else {
							cmbTransferOut.setDisable(true);
						}

					}
				});

	}

	/**
	 * sets custom cell value factory for the Combo cmb
	 * 
	 * @param cmb
	 */
	public void initPropertyCellFactory(ComboBox<facilities> cmb) {
		cmb.setCellFactory(new Callback<ListView<facilities>, ListCell<facilities>>() {
			@Override
			public ListCell<facilities> call(ListView<facilities> arg0) {
				ListCell<facilities> cell = new ListCell<facilities>() {
					@Override
					protected void updateItem(facilities facility, boolean empty) {
						super.updateItem(facility, empty);
						if (facility != null) {
							setText(facility.getName());
						}
					}
				};
				return cell;
			}
		});

		cmb.setConverter(new StringConverter<facilities>() {
			@Override
			public String toString(facilities facility) {
				if (facility == null) {
					return null;
				} else {
					return facility.getName();
				}
			}

			@Override
			public facilities fromString(String id) {
				return null;
			}
		});
	}

	/**
	 *
	 */
	@FXML
	public void handleSaveAdjustment(ActionEvent event) {
		StringBuilder messageBuilder = new StringBuilder();
		Dialogs myDialog = Dialogs.create().owner(dialogStage)
				.title("Store Product Adjustments");
		if (AdjustmentUtil.getInstance().getStockControlCardList() != null) {
			
			if (transferInOrOutCellEdited || chkTransferIn.isSelected()
					|| chkTransferOut.isSelected()) {
				
				try {
					fromFacility = cmbTransferIn.getValue().getName();
					validForm = true;
				} catch (NullPointerException ex) {
					messageBuilder
							.append("- Please select the facility from which you are Receiveing!");
					validForm = false;
				}

				try {
					toFacility = cmbTransferOut.getValue().getName();
					validForm = true;
				} catch (NullPointerException ex) {
					messageBuilder
							.append("\n- Please select the facility to which you are transering to!");
					validForm = false;
				}

			}

			if (validForm) {
				String message = "";
				myDialog.masthead("Information");
				// insert the stock control cards
				int status = ServiceResolver
						.getServiceFactory()
						.getStoreServices()
						.insertStockControlCardProducts(
								AdjustmentUtil.getInstance()
										.getStockControlCardList());
				adjustmentSuccessful = status == 1 ? true : false;
				if (status == 1) {
					message = "Adjustment successfully saved!";
					myDialog.message(message);
					myDialog.showInformation();
				} else {
					message = "Oops! something went wrong";
					myDialog.message(message);
					myDialog.showError();
				}
				// close the adjustment window
				dialogStage.close();

			} else {
				if (AdjustmentUtil.getInstance().getNetBalance() < 0) {
					messageBuilder
							.append("You are doing a Negative adjustment, please fix those before trying to save!");
				}else{
					messageBuilder
					.append("You are doing a Invalid adjustment, please fix those before trying to save!");
				}
				myDialog.masthead("Error Dialog").message(
						messageBuilder.toString());
				myDialog.showError();
			}
		} else {
			myDialog.masthead("Warning Dialog").message(
					"You haven't entered anything yet!");
			myDialog.showWarning();
		}

	}
	
	
	public static boolean isAdjustmentSuccessful(){
		return adjustmentSuccessful;
	}
	
	
	public static List<Elmis_Stock_Control_Card> getAdjustmentStockControlCardList(){
		return AdjustmentUtil.getInstance().getStockControlCardList();
	}

	/**
	 * editable number cell factory
	 * 
	 * @author mawoke
	 *
	 */
	class AdjustmentQuantityCell extends NumberEditingCell {

		@Override
		public void createTextField() {
			textField = new IntegerTextField();
			textField.setText(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setStyle("-fx-text-alignment : right");
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						Double quantity = 0.0;
						try {
							quantity= Double.valueOf(textField.getText());
						}catch(Exception ex){
							quantity = 0.0;
						}								
						commitEdit(quantity);
						// update the value of the quantity received field
						tblAdjustments.getItems().get(getIndex())
								.setAdjustmentQty(quantity);

						hasPendingChanges = true;

						if (adjustmentDate == null) {
							adjustmentDate = new Date();
						}

						// check if the transferIn/ transferOut cell is what is
						// editing now
						// and set a flag to validate on submit
						// if the user configured the transferIn/ transferOut
						// fields to a
						// different name, this check will fail
						if (tblAdjustments.getItems().get(getIndex())
								.getAdjustmentType().toLowerCase()
								.contains("transfer")) {
							transferInOrOutCellEdited = true;
						}

						validForm = AdjustmentUtil.getInstance()
								.isAdjustmentValid(tblAdjustments.getItems(),
										selectedProduct, fromFacility,
										toFacility, adjustmentDate, adjustmentFromPhyCount);

						lblStockQuantity.setText("After Adjustment : "
								+ AdjustmentUtil.getInstance().getNetBalance());

						if (validForm) {
							// remove red background
							for (Node cell : getTableView().lookupAll(
									".invalid-adjustment-bg")) {
								log.info("clearing cell " + cell.toString());
								cell.getStyleClass().remove(
										"invalid-adjustment-bg");
							}

						} else {
							// display a message
							getStyleClass().add("invalid-adjustment-bg");
						}

					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
		}

		/**
		 * return cell factory for quantity received column, that sets the
		 * custom desired cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, Integer>, TableCell<FacilityApprovedProductResult, Integer>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new AdjustmentQuantityCell();
				}
			};
		}

	}

	/**
	 * editable text cell factory
	 * 
	 * @author mawoke
	 *
	 */
	class AdjustmentRemarkCell extends StringEditingCell {

		@Override
		public void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()
					* 2);
			textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						String remark = textField.getText();
						commitEdit(remark);
						// update the value of the quantity received field
						tblAdjustments.getItems().get(getIndex())
								.setAdjustmentRemark(remark);
					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
					}
				}
			});
			textField.focusedProperty().addListener(
					new ChangeListener<Boolean>() {
						@Override
						public void changed(
								ObservableValue<? extends Boolean> observable,
								Boolean oldValue, Boolean newValue) {
							if (!newValue && textField != null) {
								commitEdit(textField.getText());
							}
						}
					});
		}

		/**
		 * return cell factory for remark column, that sets the custom desired
		 * cell behavior
		 * 
		 * @return
		 */
		@Override
		public Callback getCellFactory() {
			return new Callback<TableColumn<FacilityApprovedProductResult, Date>, TableCell<FacilityApprovedProductResult, Date>>() {
				@Override
				public TableCell call(TableColumn p) {
					return new AdjustmentRemarkCell();
				}
			};
		}

	}
}
