package org.elmis.views.store;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.log4j.Logger;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.common.plugins.DeleteButtonCell;
import org.elmis.common.plugins.MaxMinDatePickerCellFactory;
import org.elmis.common.validation.ValidationUtil;
import org.elmis.event.FilterProductsListener;
import org.elmis.facade.ProductFacade;
import org.elmis.model.Node;
import org.elmis.model.Program;
import org.elmis.model.TransactionProduct;
import org.elmis.model.User;
import org.elmis.resolver.ServiceResolver;
import org.elmis.service.interfaces.ICommon;
import org.elmis.service.interfaces.IProduct;
import org.elmis.service.interfaces.IStore;
import org.elmis.service.ws.client.request.IssueProductsRequest;
import org.elmis.service.ws.client.response.FacilityApprovedProductResult;

public class IssueProductsController extends DialogWindow implements
		Initializable {

	private static final Logger log = Logger
			.getLogger(IssueProductsController.class);

	ICommon commonService = ServiceResolver.getInstance().getServiceFactory()
			.getCommonService();
	IProduct productService = ServiceResolver.getInstance().getServiceFactory()
			.getProductService();
	IStore storeService = ServiceResolver.getInstance().getServiceFactory()
			.getStoreService();
	
	ProductFacade productFacade = ProductFacade.getInstance();
	
	public static Integer nodeId;

	@FXML
	private DatePicker dPickerIssueDate;

//	@FXML
//	private ComboBox<Site_Dispensing_point> cmbDispensingPoint;
//
	@FXML
	private ComboBox<User> cmbReceivedBy;

	@FXML
	private ListView<Program> lstViewPrograms;

	@FXML
	private TabPane tabPaneProducts;

	@FXML
	private Tab tabProgramAreas;

	@FXML
	private Tab tabProducts;

	@FXML
	private TableView<FacilityApprovedProductResult> tblIssuedProducts;

	private ObservableList<FacilityApprovedProductResult> observableIssuedProductList;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProdIssuedProductName;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Integer> colProdIssuedProductQty;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Boolean> colProdIssuedDelete;

	@FXML
	private TextField txtProductSearch;

	@FXML
	private TableView<FacilityApprovedProductResult> tblFacilityApprovedProducts;

	private ObservableList<FacilityApprovedProductResult> observableProductList;
	private FilteredList<FacilityApprovedProductResult> filteredProductList;

	@FXML
	private TableColumn<FacilityApprovedProductResult, Integer> colProductCode;

	@FXML
	private TableColumn<FacilityApprovedProductResult, String> colProductName;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		dPickerIssueDate.setDayCellFactory(new MaxMinDatePickerCellFactory(null, LocalDate.now()));
		// populate the receiving user combo
		populateCmbReceivedBy();
		// populate program areas
		populateProgramAreas();
		// initialize issued products table
		initIssuedProductsTable();
		// initialize products table
		initProductsTable();

	}

	/**
	 * populates Program Areas
	 */
	private void populateProgramAreas() {

		List<Program> programCodes = commonService.getPrograms();

		lstViewPrograms.setItems(FXCollections.observableList(programCodes));

		lstViewPrograms
				.setCellFactory(new Callback<ListView<Program>, ListCell<Program>>() {

					@Override
					public ListCell<Program> call(ListView<Program> arg0) {
						ListCell<Program> cell = new ListCell<Program>() {

							@Override
							protected void updateItem(Program program,
									boolean empty) {
								super.updateItem(program, empty);
								if (program != null) {
									setText(program.getCode());
								}
							}
						};
						return cell;
					}

				});
		
		lstViewPrograms.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Program>() {

					@Override
					public void changed(
							ObservableValue<? extends Program> observable,
							Program oldProgramCode, Program newProgramCode) {
						
						// set program area for the quantity entering dialog
						KeyPadController.programCode = newProgramCode.getCode();								

						ArrayList<FacilityApprovedProductResult> fapList = productFacade.convertToFacilityApprovedProductResult(productService.getFacilityApprovedNodeProducts(newProgramCode.getCode()));

						observableProductList = FXCollections
								.observableList(fapList);

						filteredProductList = new FilteredList<>(
								observableProductList);

						// initially show all the products
						// this is very important
						filteredProductList
								.setPredicate(new Predicate<FacilityApprovedProductResult>() {
									@Override
									public boolean test(
											FacilityApprovedProductResult product) {
										return true;
									}
								});

						SortedList<FacilityApprovedProductResult> sortedData = new SortedList<>(
								filteredProductList);
						sortedData.comparatorProperty().bind(
								tblFacilityApprovedProducts
										.comparatorProperty());
						// initialize filter products by name/code
						// text field
						initFilterByProductName();
						// bind table data to the sorted list
						tblFacilityApprovedProducts.setItems(sortedData);
						
						// open the products tab
						tabPaneProducts.getSelectionModel().select(tabProducts);
						// display the products tab
						tabProducts.setStyle("visibility : true");
						
					}
				});
	}

	/**
	 * 
	 */
	private void populateCmbReceivedBy() {
		List<User> usersList = new ArrayList<>();
		cmbReceivedBy.setItems(FXCollections.observableList(usersList));
		cmbReceivedBy
				.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {
					@Override
					public ListCell<User> call(ListView<User> arg0) {
						ListCell<User> cell = new ListCell<User>() {
							@Override
							protected void updateItem(User user, boolean empty) {
								super.updateItem(user, empty);
								if (user != null) {
									setText(user.getFirstname() + " "
											+ user.getLastname());
								}
							}
						};
						return cell;
					}
				});

		cmbReceivedBy.setConverter(new StringConverter<User>() {
			@Override
			public String toString(User user) {
				if (user == null) {
					return null;
				} else {
					return user.getFirstname() + " " + user.getLastname();
				}
			}

			@Override
			public User fromString(String id) {
				return null;
			}
		});
	}

	/**
	 * 
	 * @param cancelEvent
	 */
	@FXML
	public void handleCancelIssuing(ActionEvent cancelEvent) {
		if (tblIssuedProducts.getItems().size() > 0) {
			Action response = Dialogs
					.create()
					.owner(ELMISFacility.mainStage)
					.title("Warning")
					.masthead("Confirm Dialog")
					.message("Are you sure, you want to close with out saving?")
					.showConfirm();
			if (response == Dialog.Actions.YES) {
				this.dialogStage.close();
			}
		}

	}

	/**
	 * 
	 * @param finishIssueEvent
	 */
	@FXML
	public void handleFinishIssuing(ActionEvent finishIssueEvent) {

		if (tblIssuedProducts.getItems().size() == 0) {

			Dialogs myDialog = Dialogs.create().owner(dialogStage)
					.title("Issue products").masthead("Error Dialog")
					.message("You have not selected a product to Issue!");
			myDialog.showError();

		} else {
			ValidationUtil.resetValidtor();
			// add validation
			ValidationUtil.register(dPickerIssueDate,
					ValidationUtil.Type.REQUIRED, "Issue Date");

//			ValidationUtil.register(cmbReceivedBy,
//					ValidationUtil.Type.REQUIRED, "Received by");
			if (ValidationUtil.isValid()) {
				
				boolean insertSucceeded = false;
				
				IssueProductsRequest issueProductsRequest = new IssueProductsRequest();
				//THE Store Node
				//HARD CODED should be removed
				//TODO
				Node myNode = new Node();
				myNode.setId(30);
				
				Node dispensingPoint = new Node();
				dispensingPoint.setId(nodeId);
				
				//The Transaction Products
				ArrayList<TransactionProduct> productsIssued = new ArrayList<>();
				
				TransactionProduct issuedProduct;					
				
				
				for (FacilityApprovedProductResult product : tblIssuedProducts
						.getItems()) {						
					if (product.getQuantity() != null) {
						issuedProduct = new TransactionProduct();
						issuedProduct.setProductId(product.getProductId());
						issuedProduct.setQuantity(BigDecimal.valueOf(product.getQuantity()));
						//add the transaction product to the transaction list
						productsIssued.add(issuedProduct);
					}
				}					
				
				issueProductsRequest.setStore(myNode);
				issueProductsRequest.setDispensingPoint(dispensingPoint);
				issueProductsRequest.setProductsIssued(productsIssued);
				issueProductsRequest.setTxnDate(new Date().getTime());
				issueProductsRequest.setUserId(null);
				
				if(issueProductsRequest.getProductsIssued().size() > 0){
					insertSucceeded = storeService.issueProducts(issueProductsRequest) != -1  ? true : false;
				}
				// show successful insertion message
				if (insertSucceeded) {
					Dialogs myDialog = Dialogs.create().owner(dialogStage)
							.title("Issue products")
							.masthead("Information Dialog")
							.message("Products saved successfully!");
					myDialog.showInformation();
					dialogStage.close();
				}
				
				log.debug("started show issue voucher");
				// showIssueVoucher();				
				// clear the issued products table
				tblIssuedProducts.getItems().clear();

			} else {
				Dialogs.create().owner(ELMISFacility.mainStage).title("Error")
						.masthead("Error Dialog")
						.message(ValidationUtil.message()).showError();
			}
		}

	}

	/**
	 * 
	 * @param previewEvent
	 */
	@FXML
	public void handlePreview(ActionEvent previewEvent) {
		Long startedAt = System.currentTimeMillis();
		log.debug("started show issue voucher");
		// showIssueVoucher();
		Long endedAt = System.currentTimeMillis();
		Long timeTaken = endedAt - startedAt;
		log.debug("ended showing issue voucher in (" + (double) timeTaken
				/ (365 * 24 * 60 * 60) + " Seconds)");
	}

	/**
	 * 
	 */
	private void initIssuedProductsTable() {
		colProdIssuedProductName
				.setCellValueFactory(new PropertyValueFactory<>("productName"));
		colProdIssuedProductQty.setCellValueFactory(new PropertyValueFactory<>(
				"quantity"));

		colProdIssuedDelete
				.setCellFactory(new Callback<TableColumn<FacilityApprovedProductResult, Boolean>, TableCell<FacilityApprovedProductResult, Boolean>>() {

					@Override
					public TableCell call(
							TableColumn<FacilityApprovedProductResult, Boolean> p) {
						return new DeleteButtonCell();
					}

				});

		observableIssuedProductList = FXCollections.observableArrayList();
		tblIssuedProducts.setItems(observableIssuedProductList);
	}

	/**
	 * 
	 */
	private void initProductsTable() {
		colProductName.setCellValueFactory(new PropertyValueFactory<>(
				"productName"));
		colProductCode.setCellValueFactory(new PropertyValueFactory<>(
				"productCode"));

		// add selection change listener		
		tblFacilityApprovedProducts.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FacilityApprovedProductResult> (){

			@Override
			public void changed(
					ObservableValue<? extends FacilityApprovedProductResult> observable,
					FacilityApprovedProductResult oldProduct,
					FacilityApprovedProductResult newProduct) {
				

				if (null != newProduct) {
					boolean noStock = false;
					if (newProduct.getQuantity() > 0) {
						KeyPadController.quantityInStock = newProduct.getQuantity();
						DialogWindow
								.getInstance()
								.openDialog(
										ELMISFacility.class
												.getResource("views/store/KeyPad.fxml"),
										"Please Enter Quantity",
										dialogStage);
						if (KeyPadController.getEnteredQuantity() != 0) {
							newProduct.setQuantity(KeyPadController
									.getEnteredQuantity());
							log.debug("Listened the click on row");
							observableIssuedProductList
									.add(newProduct);
							KeyPadController.clear();
						}
					} else {
						noStock = true;
					}
					if (noStock) {
						Dialogs myDialog = Dialogs
								.create()
								.owner(dialogStage)
								.title("Issuing Products")
								.masthead("Information Dialog")
								.message(
										"Sorry, cannot continue to issue, this product is out of stock!");

						myDialog.showWarning();
					}

				}
			}});

	}

	/**
	 * 
	 */
	private void initFilterByProductName() {
		txtProductSearch.textProperty().addListener(
				new FilterProductsListener(filteredProductList));
	}
	/**
	 * 
	 */
//	private void showIssueVoucher() {
//		/**
//		 * Display the issue voucher
//		 * 
//		 */
//		List<Items_issue> list = new ArrayList<>();
//
//		Items_issue issuedItem;
//
//		for (FacilityApprovedProductResult product : tblIssuedProducts
//				.getItems()) {
//
//			issuedItem = new Items_issue();
//
//			issuedItem.setProduct_code(product.getProductCode());
//			issuedItem.setProduct_id(0);
//			issuedItem.setProduct_name(product.getProductName());
//			issuedItem.setQtyToIssue(Double.valueOf((product.getQuantity())));
//			if (dPickerIssueDate.getValue() != null) {
//				issuedItem.setCreateddate(DateUtil
//						.fromLocalDate(dPickerIssueDate.getValue()));
//			} else {
//				issuedItem.setCreateddate(new Date());
//			}
//
//			list.add(issuedItem);
//		}
//
//		try {
//
//			Map<String, Object> parameterMap = new HashMap<>();
//
//			parameterMap.put("bg_text", "");
//
//			users receivedBy = cmbReceivedBy.getValue();
//			StringBuilder receivedBySB = new StringBuilder("");
//			if (receivedBy != null) {
//				receivedBySB = new StringBuilder()
//						.append(receivedBy.getFirstname()).append(" ")
//						.append(receivedBy.getLastname());
//				parameterMap.put("receivedby", receivedBySB.toString());
//			} else {
//				parameterMap.put("receivedby", "PLACEHOLDER");
//			}
//			/**
//			 * REMOVE HARD CODED
//			 */
//			parameterMap.put("issued_by", "Mekbib Awoke");
//			parameterMap.put("receivedby", receivedBySB.toString());
//			if (cmbDispensingPoint.getValue() != null) {
//				parameterMap.put("dispensing_point", cmbDispensingPoint
//						.getValue().getDispensingpointname());
//			} else {
//				parameterMap.put("dispensing_point", "PLACEHOLDER");
//			}
//
//			JasperPrint print = JasperFillManager.fillReport(
//					"Reports/items_issue.jasper", parameterMap,
//					new JRBeanCollectionDataSource(list));
//
//			JasperViewer.viewReport(print, false);
//
//		} catch (JRException e) {
//			log.error(e.getMessage());
//		}
//	}
}
