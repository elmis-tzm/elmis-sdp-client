/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.arv;

import java.sql.Timestamp;
import java.util.Date;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import org.controlsfx.dialog.Dialogs;
import org.elmis.common.DialogWindow;
import org.elmis.common.converters.DateConverter;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.service.interfaces.IARVDispensing;
import org.elmis.resolver.ServiceResolver;
import org.elmis.util.ARTNumberGenerator;

import extfx.scene.control.DatePicker;

/**
 * FXML Controller class
 *
 * @author Mekbib
 */
public class RegisterARVClientController extends DialogWindow {
    
    //a VBox containing the datepicker field for the birthday
    @FXML
    private VBox vboxBirthday;

    //birthday datepicker
    private DatePicker datePickerBirthday;

    //a VBox containing the datepicker field for the birthday
    @FXML
    private VBox vboxRegistrationDay;

    //registration date datepicker
    private DatePicker datePickerRegistrationDate;

    //art number
    @FXML
    private TextField txtArtNumber;

    //nrc number
    @FXML
    private TextField txtNrcNumber;

    //gender
    @FXML
    private ComboBox cmbGender;

    //name
    @FXML
    private TextField txtFirstName;

    //lastname
    @FXML
    private TextField txtLastName;  
    /**
     * 
     * @param artNumber 
     */
    public void initialize(String artNumber) {
        
        txtArtNumber.setText(artNumber);
        
        cmbGender.getItems().addAll("Male", "Female");
        cmbGender.getSelectionModel().select(0);

        //create custom datepicker and
        datePickerBirthday = new DatePicker();
        vboxBirthday.getChildren().add(datePickerBirthday);
        
        datePickerRegistrationDate = new DatePicker();
        datePickerRegistrationDate.setMaxDate(new Date());
        vboxRegistrationDay.getChildren().add(datePickerRegistrationDate);
        
    }
    
    public boolean handleEditClientSave() {
        
    	IARVDispensing arvDispesingService = ServiceResolver.getServiceFactory().getARVDispensing();
        Elmis_Patient newElmisPatient = new Elmis_Patient();
        
        newElmisPatient.setId(txtArtNumber.getText());
        String formattedARTNumber = ARTNumberGenerator.generateARTNumer(txtArtNumber.getText());
        newElmisPatient.setArt_number(formattedARTNumber);
        newElmisPatient.setNrc_number(txtNrcNumber.getText());
        // newElmisPatient.setAge(null);
        newElmisPatient.setDateofbirth(datePickerBirthday.getValue());
        
        Timestamp registrationTime = DateConverter.fromDateToTimestamp(datePickerRegistrationDate.getValue());
        
        newElmisPatient.setRegistration_date(registrationTime);
        newElmisPatient.setFirstname(txtFirstName.getText());
        newElmisPatient.setLastname(txtLastName.getText());
        newElmisPatient.setSex(cmbGender.getValue().toString());
        
        int status = arvDispesingService.registerClient(newElmisPatient);
        if(status == 1){
            //show a success message
            Dialogs.create().owner(dialogStage).title("New Patient Form")
                        .masthead("Information Dialog")
                        .message("Patient Saved Successfully!")
                        .showInformation();
            dialogStage.close();
        }else{
            //show failure message
            //show a success message
            Dialogs.create().owner(dialogStage).title("New Patient Form")
                        .masthead("Error Dialog")
                        .message("There was an error, try again please!")
                        .showError();
            dialogStage.close();
        }
        
        return false;
    }
    
}
