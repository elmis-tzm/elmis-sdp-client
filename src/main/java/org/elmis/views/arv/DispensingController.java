/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.arv;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.ELMISFacility;
import org.elmis.common.constants.Screens;
import org.elmis.common.plugins.CheckBoxCellFactory;
import org.elmis.common.plugins.TextFieldLimitCharsInputListener;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.service.interfaces.IARVDispensing;
import org.elmis.fx.model.Regimen;
import org.elmis.fx.services.RegimenService;
import org.elmis.resolver.ServiceResolver;
import org.elmis.screenframework.ControlledScreen;
import org.elmis.screenframework.ScreensController;
import org.elmis.util.ARTNumberGenerator;
import org.thehecklers.dialogfx.DialogFX;
import org.thehecklers.dialogfx.DialogFX.Type;

/**
 *
 * @author Mekbib
 */
public class DispensingController extends ClientSearchComponents implements Initializable, ControlledScreen {

    //this parents this screen and controls its visibility on the scene
    ScreensController myController;
    ObservableList<Tab> arvDispesingTabs;
    List arvDispensingTabsList;

    //register a screen controller
    @Override
    public void setScreenParent(ScreensController screenPage) {
        myController = screenPage;
    }

    //will be called when the client search tab screen gets displayed
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //add custom components on the fly
        hboxDateDispensed.getChildren().add(dateDispensedDate);
        dateDispensedDate.setValue(new Date());

        //take out all tabs that we don't want to show at the first screen
        tabPaneDispensing.getTabs().remove(tabCurrentRegimen);
        tabPaneDispensing.getTabs().remove(tabEditClient);
        tabPaneDispensing.getTabs().remove(tabAdvancedClientSearch);

        //register a changeListener to textProperty of txtArtNumber
        TextFieldLimitCharsInputListener noOfCharsinputListener = new TextFieldLimitCharsInputListener(txtArtNumber);
        //limit the number of characters to be input in txtArtNumber
        noOfCharsinputListener.setMaxLength(13);

    }

    //will be called when btnSearch is clicked
    //initialize regimen table
    public void searchActionHandler(ActionEvent evt) {
        //write an implementation of the action handler 
        //for the button to which this method is assigned to 
        if (txtArtNumber.getText().equals("")) {
            DialogFX dialog = new DialogFX(Type.ERROR);
            dialog.setTitleText("Client Search Error");
            dialog.setMessage("Client ART Number required!");
            dialog.showDialog();
        } else if (txtArtNumber.getText().length() < 13) {
            DialogFX dialog = new DialogFX(Type.ERROR);
            dialog.setTitleText("Client Search Error");
            dialog.setMessage("Invalid Client ART Number, it must be 15 digits!");
            dialog.showDialog();
        } else if (txtArtNumber.getText().length() == 13) {

            String artNumber = txtArtNumber.getText();
            String formattedARTNumber = ARTNumberGenerator.generateARTNumer(artNumber);

            IARVDispensing arvDispesingService = ServiceResolver.getServiceFactory().getARVDispensing();
            
            System.out.println("SEARCHING FOR PATIENT WITH ART NUMBER + "
                    + formattedARTNumber);
            Elmis_Patient arvPatient = arvDispesingService.getARVClientByART(
                    formattedARTNumber);
            
            

            if (null != arvPatient) {
                System.out.println("ARV Patient " + arvPatient.toString());
                //we have a valid ART Number search client associate with that            
                colStatus.setCellFactory(new CheckBoxCellFactory());
                colStatus.setCellValueFactory(new PropertyValueFactory<Regimen, Boolean>("status"));
                colProductCode.setCellValueFactory(new PropertyValueFactory<Regimen, String>("productCode"));
                colProductName.setCellValueFactory(new PropertyValueFactory<Regimen, String>("productName"));
                colDosage.setCellValueFactory(new PropertyValueFactory<Regimen, String>("dosage"));
                colQuantity.setCellValueFactory(new PropertyValueFactory<Regimen, Float>("quantity"));
                colBottlesOrPacks.setCellValueFactory(new PropertyValueFactory<Regimen, Float>("bottlesOrPacks"));
                colBalance.setCellValueFactory(new PropertyValueFactory<Regimen, Float>("balance"));

                //get regimen associated this client id
                //MAKE CALL TO THE SERVICE LAYER THAT HAS BEEN INTRODUCED BY THE REFACTORING    
                ObservableList<Regimen> regimenList = RegimenService.getRegimenByArtNumber(txtArtNumber.getText());
                //populate regimen table with client's regimen

                tblViewCurrentRegimen.setEditable(true);
                tblViewCurrentRegimen.getItems().setAll(regimenList);

                //bring the current regimen tab to view
                tabPaneDispensing.getTabs().add(tabCurrentRegimen);
                tabPaneDispensing.getSelectionModel().select(tabCurrentRegimen);
            } else {
                Action response = Dialogs.create().owner(ELMISFacility.mainStage).title("Do you want to register a new patient")
                        .masthead("Confirm Dialog")
                        .message("Do you want to register a new patient?")
                        .showConfirm();
                if (response == Dialog.Actions.YES) {
                        //passing null means the form will not be ready for 
                    //entering a new patient
                    editARVClient(null);
                }
            }
        }

    }

    //will be called when btnDetailedSearch is clicked
    public boolean detailedSearchActionHandler(ActionEvent evt) {

        try {
            // Load the fxml file and create a new stage for the popup
            FXMLLoader loader = new FXMLLoader(ELMISFacility.class.getResource("views/arv/dispensing/AdvancedClientSearch.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Advanced Client Search");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setResizable(false);
            dialogStage.initOwner(ELMISFacility.mainStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AdvancedClientSearchController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
            return false;
        }
    }

    /**
     * will create or update patient depending on the value of the object
     *
     * @param patient
     * @return
     */
    public boolean editARVClient(Elmis_Patient patient) {

        try {
            // Load the fxml file and create a new stage for the popup
            FXMLLoader loader = new FXMLLoader(ELMISFacility.class.getResource("views/arv/dispensing/RegisterARVClient.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Initiate/ Change Regimen");
            dialogStage.setResizable(false);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(ELMISFacility.mainStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            RegisterARVClientController controller = loader.getController();            
            controller.setDialogStage(dialogStage);
            
            //call the init method for the register client screen
            controller.initialize(txtArtNumber.getText());

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
            return false;
        }

    }

    //initiate Regimen handler
    public boolean initiateRegimen(ActionEvent initiateRegimentEvt) {
        try {
            // Load the fxml file and create a new stage for the popup
            FXMLLoader loader = new FXMLLoader(ELMISFacility.class.getResource("views/arv/dispensing/InitiateRegimen.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Initiate/ Change Regimen");
            dialogStage.setResizable(false);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(ELMISFacility.mainStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            InitiateRegimenController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded
            e.printStackTrace();
            return false;
        }
    }

    //close current regiment tab
    public void closeRegimenTab(ActionEvent e) {
        tabPaneDispensing.getTabs().remove(tabCurrentRegimen);
        tabPaneDispensing.getSelectionModel().select(tabClientSearch);
    }

    //return user back to dashboard screen
    public void backToDashboard(ActionEvent e) {
        myController.setScreen(Screens.mainScreenID);
    }

    //remove client edit tab showing the respective message
    public void handleEditClientSave(ActionEvent clientEditSaveEvent) {

        DialogFX dialog = new DialogFX(Type.INFO);
        dialog.setTitleText("Client information");
        dialog.setMessage("Client information updated successfully!");
        dialog.showDialog();

        tabPaneDispensing.getTabs().remove(tabEditClient);
        tabPaneDispensing.getSelectionModel().select(tabClientSearch);

    }
}
