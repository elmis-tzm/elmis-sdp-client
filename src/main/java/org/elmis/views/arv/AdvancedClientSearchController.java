/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.arv;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;

import org.elmis.common.DialogWindow;

/**
 * FXML Controller class
 *
 * @author Mekbib
 */
public class AdvancedClientSearchController extends DialogWindow implements Initializable {
   
    @FXML
    ComboBox cmbAdvSearchGender;
    

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cmbAdvSearchGender.getItems().addAll("Male", "Female");
        cmbAdvSearchGender.getSelectionModel().select(0);
    }

    

}
