/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.arv;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import org.elmis.common.DialogWindow;
import org.elmis.fx.model.Regimen;

import extfx.scene.control.DatePicker;

/**
 *
 * @author Mekbib
 */
public abstract class ClientSearchComponents extends DialogWindow {    

    //the arvDespensing tab pane
    @FXML
    TabPane tabPaneDispensing = new TabPane();

    //the client search tab
    @FXML
    Tab tabClientSearch = new Tab();
    
    //ArtNumber textfield
    @FXML
    TextField txtArtNumber;

    //NrcNumber textfield
    @FXML
    TextField txtNrcNumber;

    //NRCField one
    @FXML
    TextField txtNRCFieldThree;

    //the regimen tab
    @FXML
    Tab tabCurrentRegimen = new Tab();
    
    //the edit client tab
    @FXML
    Tab tabEditClient = new Tab();
    
    //gender combobox on the edit client form
    @FXML
    ComboBox cmbEditGender;

    //the edit client tab
    @FXML
    Tab tabAdvancedClientSearch = new Tab();

    //hbox containing dispensing date
    @FXML
    HBox hboxDateDispensed = new HBox();

    //the dispensed date
    @FXML
    DatePicker dateDispensedDate = new DatePicker();

    

    //close regimen button
    @FXML
    Button btnRegimenClose = new Button();

    /********************************************************
     * 
     * the following are columns of the regimen table view
     * 
     *********************************************************/    
    @FXML
    TableView<Regimen> tblViewCurrentRegimen = new TableView();
    @FXML
    TableColumn<Regimen, Boolean> colStatus = new TableColumn();
    @FXML
    TableColumn<Regimen, String> colProductCode = new TableColumn();
    @FXML
    TableColumn<Regimen, String> colProductName = new TableColumn();
    @FXML
    TableColumn<Regimen, String> colDosage = new TableColumn();
    @FXML
    TableColumn<Regimen, Float> colQuantity = new TableColumn();
    @FXML
    TableColumn<Regimen, Float> colBottlesOrPacks = new TableColumn();
    @FXML
    TableColumn<Regimen, Float> colBalance = new TableColumn();
    /***************************************************
     * end of columns of the regimen table view
     * *************************************************/
}
