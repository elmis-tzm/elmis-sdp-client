/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.views.dashboard;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import org.elmis.ELMISFacility;
import org.elmis.common.DialogWindow;
import org.elmis.screenframework.ControlledScreen;
import org.elmis.screenframework.ScreensController;

/**
 *
 * @author Mekbib
 */
public class DashboardController implements Initializable, ControlledScreen {

	ScreensController myController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		myController = screenPage;
	}
	
	
	
	/*****************************************************************
	*
	* ARV DISPENSING MODULE DASHBOARD MENU BUTTONS LISTENERS
	*
	******************************************************************/
	@FXML 
	public void arvDispenseBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/arv/dispensing/Dispensing.fxml"),
				"ARV Client Search", ELMISFacility.mainStage);
	}
	
	@FXML
	public void arvPhysicalCountBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/arv/physicalcount/PhysicalCount.fxml"),
				"Record Product Physical Count", ELMISFacility.mainStage);
	}

	public void productStatusBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/arv/productstatus/ProductStatus.fxml"),
				"ARV Product Transactions", ELMISFacility.mainStage);
	}

	@FXML
	public void arvAdjustmentBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/arv/adjustment/AdjustmentList.fxml"),
				"Record Product Adjustments", ELMISFacility.mainStage);
	}
	
	
	
	
	/*****************************************************************
	*
	* STORE MODULE DASHBOARD MENU BUTTONS LISTENERS
	*
	******************************************************************/
	@FXML
	public void issueItemsBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/store/IssueToList.fxml"),
				"Select Dispensing Point", ELMISFacility.mainStage);
	}
	
	@FXML
	public void receiveItemsBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/store/ReceiveFromList.fxml"),
				"Select Product Source", ELMISFacility.mainStage);
	}
	@FXML
	public void stockControlCardBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/store/StockControlCard.fxml"),
				"Stock Control Card", ELMISFacility.mainStage);
	}
	
	@FXML
	public void storeAdjustmentsBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/store/AdjustmentList.fxml"),
				"Record Product Adjustments", ELMISFacility.mainStage);
	}
	
	@FXML
	public void storePhysicalCountBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/store/PhysicalCount.fxml"),
				"Record Product Physical Count", ELMISFacility.mainStage);
	}

	@FXML
	public void productSourceBtnActionHandler(ActionEvent e) {
		DialogWindow.getInstance().openDialog(
				ELMISFacility.class
						.getResource("views/admin/productsource/ProductSource.fxml"),
				"Product Source", ELMISFacility.mainStage);
	}

	

}
