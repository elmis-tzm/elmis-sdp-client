package org.elmis.facade;

import java.util.ArrayList;
import java.util.List;

import org.elmis.model.StockControlCard;

public class StockControlCardFacade {

	private static StockControlCardFacade instance = new StockControlCardFacade();

	private StockControlCardFacade() {
	}

	public static StockControlCardFacade getInstance() {
		if (null == instance) {
			instance = new StockControlCardFacade();
		}
		return instance;
	}

	public ArrayList<StockControlCard> convertToStockControlCard(List<org.elmis.model.report.StockControlCard> stcReportList){
		
		ArrayList<StockControlCard> stcList = new ArrayList<>();		
		
		StockControlCard stc = null;
		
		for (org.elmis.model.report.StockControlCard stcReport : stcReportList) {
			
			stc = new StockControlCard();
			
			stc.setBalance(stcReport.getBalance().doubleValue());
			stc.setCreateddate(stcReport.getDate());
			if(stcReport.getQtyIssued() != null){
				stc.setQty_isssued(stcReport.getQtyIssued().doubleValue());
			}
			if(stcReport.getQtyReceived() != null){
				stc.setQty_received(stcReport.getQtyReceived().doubleValue());
			}			
			stc.setIssueto_receivedfrom(stcReport.getFromOrTo());
			stcReport.setRefNo(stcReport.getRefNo());
			stcReport.setRemark(stcReport.getRemark());
			
			stcList.add(stc);
		}
		
		return stcList;
	}


}
