package org.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

public class FacilityApprovedProduct {
    private Integer id;

    private Integer facilitytypeid;

    private Integer programproductid;

    private Integer maxmonthsofstock;

    private BigDecimal minmonthsofstock;

    private BigDecimal eop;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFacilitytypeid() {
        return facilitytypeid;
    }

    public void setFacilitytypeid(Integer facilitytypeid) {
        this.facilitytypeid = facilitytypeid;
    }

    public Integer getProgramproductid() {
        return programproductid;
    }

    public void setProgramproductid(Integer programproductid) {
        this.programproductid = programproductid;
    }

    public Integer getMaxmonthsofstock() {
        return maxmonthsofstock;
    }

    public void setMaxmonthsofstock(Integer maxmonthsofstock) {
        this.maxmonthsofstock = maxmonthsofstock;
    }

    public BigDecimal getMinmonthsofstock() {
        return minmonthsofstock;
    }

    public void setMinmonthsofstock(BigDecimal minmonthsofstock) {
        this.minmonthsofstock = minmonthsofstock;
    }

    public BigDecimal getEop() {
        return eop;
    }

    public void setEop(BigDecimal eop) {
        this.eop = eop;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }
}