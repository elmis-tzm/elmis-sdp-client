package org.elmis.model;

import java.util.List;

public class Node {
    private Integer id;

    private String name;

    private String description;

    private Integer type;
    
    private List<NodeProduct> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    
    public List<NodeProduct> getProducts() {
		return products;
	}
    
    public void setProducts(List<NodeProduct> products) {
		this.products = products;
	}
}