package org.elmis.model;

public class ProductAdjustment {
	
	private String productCode;
	private String adjustmentRemark;
	private double adjustmentQty;
	private String transferFrom;
	private String transferTo;
	private String adjustmentType;
	private Boolean additive;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getAdjustmentRemark() {
		return adjustmentRemark;
	}
	public void setAdjustmentRemark(String adjustmentRemark) {
		this.adjustmentRemark = adjustmentRemark;
	}
	public double getAdjustmentQty() {
		return adjustmentQty;
	}
	public void setAdjustmentQty(double adjustmentQty) {
		this.adjustmentQty = adjustmentQty;
	}
	public String getTransferFrom() {
		return transferFrom;
	}
	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}
	public String getTransferTo() {
		return transferTo;
	}
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	public Boolean getAdditive() {
		return additive;
	}
	public void setAdditive(Boolean additive) {
		this.additive = additive;
	}
	
	
}
