package org.elmis.model.report;

import java.math.BigDecimal;
import java.util.Date;

public class StockControlCard {
	
	private Date date;
	private String refNo;
	private String fromOrTo;
	private BigDecimal qtyReceived;
	private BigDecimal qtyIssued;
	private BigDecimal lossesNAdjustments;
	private BigDecimal balance;
	private String remark;
	private String user;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getFromOrTo() {
		return fromOrTo;
	}
	public void setFromOrTo(String fromOrTo) {
		this.fromOrTo = fromOrTo;
	}
	public BigDecimal getQtyReceived() {
		return qtyReceived;
	}
	public void setQtyReceived(BigDecimal qtyReceived) {
		this.qtyReceived = qtyReceived;
	}
	public BigDecimal getQtyIssued() {
		return qtyIssued;
	}
	public void setQtyIssued(BigDecimal qtyIssued) {
		this.qtyIssued = qtyIssued;
	}
	public BigDecimal getLossesNAdjustments() {
		return lossesNAdjustments;
	}
	public void setLossesNAdjustments(BigDecimal lossesNAdjustments) {
		this.lossesNAdjustments = lossesNAdjustments;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
}
