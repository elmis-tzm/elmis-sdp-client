package org.elmis.model;

public class HIVTestTransaction {
    private Integer id;

    private Integer hivTestId;

    private Integer transactionId;

    private Integer testTypeId;

    private String result;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHivTestId() {
        return hivTestId;
    }

    public void setHivTestId(Integer hivTestId) {
        this.hivTestId = hivTestId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(Integer testTypeId) {
        this.testTypeId = testTypeId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }
}