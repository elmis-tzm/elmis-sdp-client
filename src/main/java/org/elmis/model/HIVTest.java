package org.elmis.model;

public class HIVTest {
    private Integer id;

    private Integer nodeId;

    private Integer purposeId;

    private Integer screeningProductId;

    private Integer confirmatoryProductId;

    private Integer screeningResultId;

    private Integer confirmatoryResultId;

    private Integer finalResultId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(Integer purposeId) {
        this.purposeId = purposeId;
    }

    public Integer getScreeningProductId() {
        return screeningProductId;
    }

    public void setScreeningProductId(Integer screeningProductId) {
        this.screeningProductId = screeningProductId;
    }

    public Integer getConfirmatoryProductId() {
        return confirmatoryProductId;
    }

    public void setConfirmatoryProductId(Integer confirmatoryProductId) {
        this.confirmatoryProductId = confirmatoryProductId;
    }

    public Integer getScreeningResultId() {
        return screeningResultId;
    }

    public void setScreeningResultId(Integer screeningResultId) {
        this.screeningResultId = screeningResultId;
    }

    public Integer getConfirmatoryResultId() {
        return confirmatoryResultId;
    }

    public void setConfirmatoryResultId(Integer confirmatoryResultId) {
        this.confirmatoryResultId = confirmatoryResultId;
    }

    public Integer getFinalResultId() {
        return finalResultId;
    }

    public void setFinalResultId(Integer finalResultId) {
        this.finalResultId = finalResultId;
    }
}